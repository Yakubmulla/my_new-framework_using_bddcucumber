Feature: Trigger PatchAPI 
@Patch_API_TestCases
Scenario: Trigger the  PatchAPI request with valid request parameters 
	Given Enter NAME and JOB in Patch request body 
	When Send the Patch request with payload 
	Then Validate Patch status code 
	And Validate Patch response body parameters