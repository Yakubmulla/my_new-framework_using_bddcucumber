package Cucumber_Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith (Cucumber.class)
@CucumberOptions(features = "src/test/java/Features",glue ="Step_Definition",
tags = "@Put_API_TestCases or @Post_API_TestCases or @Patch_API_TestCases ")
public class Test_Runner {

}
