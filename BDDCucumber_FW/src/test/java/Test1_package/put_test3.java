package Test1_package;

import java.io.File;


import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Api_commen_method.Comman_method_handle_api;
import New_repository.put_request_repository;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.put_endpoint1;
import io.restassured.path.json.JsonPath;

public class put_test3 extends Comman_method_handle_api {
	static File log_dir;
	static String requestbody;
	static String endpoint;
	static String responsebody;
	 
	@BeforeTest
	public static void Test_Setup2() throws IOException {
		log_dir = handle_directory.Create_log_directory("Put_test3_log");
		 requestbody = put_request_repository.put_request();
		 endpoint = put_endpoint1.put_endpoint();
	}
	
	@Test
	public static void executor() throws IOException {
		 

		for (int i = 0; i < 5; i++) {

			int statusCode = put_statusCode(requestbody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				String responsebody = put_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				
				handle_api_logs.avidence_creator(log_dir, "putt_test3", endpoint, requestbody, responsebody);
				put_test3.validatur(requestbody, responsebody);
				break;

			} else {
				System.out.println("Expected status code not found hence retraying");
			}
		}
	}

	public static void validatur(String requestbody, String responsebody) {
		JsonPath j_req = new JsonPath(requestbody);
		String req_name = j_req.getString("name");
		String req_job = j_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsonp_res = new JsonPath(responsebody);
		String res_name = jsonp_res.getString("name");
		System.out.println("Name is :" + res_name);
		String res_id = jsonp_res.getString("id");
		System.out.println("Id is :" + res_id);
		String res_job = jsonp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_createdAt = jsonp_res.getString("createdAt");
		System.out.println("createdAt is :" + res_createdAt);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);

	}
	
	@AfterTest
	public static void  Test_Teardown2() throws IOException {
			String tesclassname  =put_test3.class.getName();
			handle_api_logs.avidence_creator(log_dir, "putt_test3", endpoint, requestbody, responsebody);
			
}
}
