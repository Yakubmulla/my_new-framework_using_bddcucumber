package Test1_package;

import java.io.File;


import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import Api_commen_method.Comman_method_handle_api;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.get_endpoint;

public class get_test4 extends Comman_method_handle_api {  
	
	static int statuscode;
	static String responsebody;
	static String endpoint;
	
	// step1 = Create the class as Get_test4 and Extend to
															                               // Comman_method_handle_api
	public static void executor() {  
		
		
		// step2 = Create a static method
		
		String endpoint = get_endpoint.get_endpoint();                                     // step3 = give the endpoint for regfran
		for (int i = 0; i < 5; i++) {                                                      // step4 = create the for loop with requirment

			int statusCode = get_statusCode(endpoint);                                    // step5: create the Varible to strore the value
			System.out.println(statusCode); // step6: print the varible
			if (statusCode == 200) { // step7: use Coditional statment

				String responsebody = get_responsebody(endpoint);                          // step8: Creating varible as A String / print the
																	                          // Varible
				System.out.println(responsebody);
				
				get_test4.get_validator(responsebody);
				; // step9 : class use the Validator
				break; // step10: use the break to stop the loop

			} else {
				System.out.println("Expected status code not found hence retraying");
			}
		}
	}

	public static void get_validator(String resposebody) { // create the main method
		int id[] = { 1, 2, 3, 4, 5, 6 }; // create The Array of index value 0,1,2,3,4,5
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"even.holt@reqres.in", "charles.morris@reqres", "even.holt@reqres.in", "charles.morris@reqres.in",
				"tracey.ramos@reqres.in" };
		String fname[] = { "George", "janet", "Emma", "Even", "Charles", "Tracey" };
		String lname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		JSONObject res_array = new JSONObject(resposebody); // with help off new keywold create the object
		JSONArray data_array = res_array.getJSONArray("data");
		int count = data_array.length();
		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_fname = fname[i];
			String exp_lname = lname[i];

			int res_id = data_array.getJSONObject(i).getInt("id");
			String res_email = data_array.getJSONObject(i).getString("email");
			String res_fname = data_array.getJSONObject(i).getString("first_name");
			String res_lname = data_array.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_fname, exp_fname);
			Assert.assertEquals(res_lname, exp_lname);

		}
	}
}