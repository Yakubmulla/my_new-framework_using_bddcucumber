package DataDrivenStepDefination;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_commen_method.Comman_method_handle_api;
import Test1_package.Post_test1;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.post_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_Data {
	File log_dir;
	String requestbody;
	String endpoint;
	String responsebody;
	int statuscode;
	
	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) {
		log_dir = handle_directory.Create_log_directory("Patch_test1_log");
		requestbody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		endpoint = post_endpoint.post_endpoint();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the patch request with data")
	public void send_the_post_request_with_data() {
		statuscode = Comman_method_handle_api.post_statusCode(requestbody, endpoint);
		responsebody = Comman_method_handle_api.post_responsebody(requestbody, endpoint);
		System.out.println(responsebody );
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch status code")
	public void validate_data_driven_post_status_code() {
		Assert.assertEquals(statuscode, 201);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch response body parameters")
	public void validate_data_driven_post_response_body_parameters() throws IOException {
		handle_api_logs.avidence_creator(log_dir, " Post_test1", endpoint, requestbody, responsebody);
		Post_test1.validatur(requestbody, responsebody);
		System.out.println("Patch_responscode is Sucssful");
		//throw new io.cucumber.java.PendingException();
	}
	
	

}
