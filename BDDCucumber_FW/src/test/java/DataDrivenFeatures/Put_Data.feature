Feature: Trigger Put_DataDriven_API

Scenario Outline: Trigger the put API request with valid request parameters
		Given Enter "<Name>" and "<Job>" in post request body
		When Send the put request with data
		Then Validate data_driven_put status code
		And Validate data_driven_put response body parameters

Examples: 
		|Name |Job |
		|yakub|QA|
		|yash|SrQA|
		|Abrar|Dev|