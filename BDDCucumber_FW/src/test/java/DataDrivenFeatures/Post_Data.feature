Feature: Trigger Post_DataDriven_API

Scenario Outline: Trigger the put API request with valid request parameters
		Given Enter "<Name>" and "<Job>" in post request body
		When Send the post request with data
		Then Validate data_driven_post status code
		And Validate data_driven_post response body parameters

Examples: 
		|Name |Job |
		|Arun|QA|
		|Anil|SrQA|
		|Anuraj|Dev|