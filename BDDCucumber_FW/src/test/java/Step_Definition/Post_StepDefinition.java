package Step_Definition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_commen_method.Comman_method_handle_api;
import New_repository.post_request_repository;
import Test1_package.Post_test1;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.post_endpoint;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Post_StepDefinition extends Comman_method_handle_api {

	File log_dir;
	String requestbody;
	String endpoint;
	String responsebody;
	int statuscode;
	@Before()
	public void setup () {
		System.out.println("Triggering Process Begins");
	}

	@Given("Enter NAME and JOB in Postrequest body")
	public void enter_name_and_job_in_postrequest_body() throws IOException {
		log_dir = handle_directory.Create_log_directory("Post_test1_log");
		requestbody = post_request_repository.post_request();
		endpoint = post_endpoint.post_endpoint();
		// throw new io.cucumber.java.PendingException();

	}

	@When("Send the Post request with payload")
	public void send_the_post_request_with_payload() {
		statuscode = Comman_method_handle_api.post_statusCode(requestbody, endpoint);
		responsebody = Comman_method_handle_api.post_responsebody(requestbody, endpoint);

		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		handle_api_logs.avidence_creator(log_dir, " Post_test1", endpoint, requestbody, responsebody);
		Post_test1.validatur(requestbody, responsebody);
		System.out.println("Post_responscode is Sucssful");
		// throw new io.cucumber.java.PendingException();

	}
	//Hooks
	@After
	public void teardown() {
		System.out.println("processEnded");
	}
		
	}
	


