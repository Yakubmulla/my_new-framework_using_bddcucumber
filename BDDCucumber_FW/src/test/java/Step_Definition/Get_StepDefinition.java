package Step_Definition;

import java.io.IOException;

import org.testng.Assert;

import Api_commen_method.Comman_method_handle_api;
import Test1_package.Post_test1;
import Utility_Common_Method.handle_api_logs;
import endpoint.get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get_StepDefinition extends Comman_method_handle_api {
	int statuscode;
	String responsebody;
	String endpoint;

	@Given("Enter endpoint")
	public void enter_endpoint() {
		endpoint = get_endpoint.get_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send Request With Valid Get Payload")
	public void send_request_with_valid_get_payload() {
		statuscode = Comman_method_handle_api.get_statusCode(endpoint);
		responsebody = Comman_method_handle_api.get_responsebody(endpoint);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get Status Code")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get  Response body parameters")
	public void validate_get_response_body_parameters() throws IOException {
		handle_api_logs.avidence_creator(null, "Get_test4 ", endpoint, responsebody, endpoint);
		Post_test1.validatur(responsebody, endpoint);
		System.out.println("Post_responscode is Sucssful");

		// throw new io.cucumber.java.PendingException();
	}

}
