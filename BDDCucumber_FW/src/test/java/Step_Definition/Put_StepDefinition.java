package Step_Definition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_commen_method.Comman_method_handle_api;
import New_repository.put_request_repository;
import Test1_package.Post_test1;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.put_endpoint1;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_StepDefinition extends Comman_method_handle_api {
	File log_dir;
	String requestbody;
	String endpoint;
	String responsebody;
	int statuscode;
	@Before()
	public void setup() {
		System.out.println("Triggering Process Begins");
	}

	@Given("Enter NAME and JOB in Put request body")
	public void enter_name_and_job_in_put_request_body() throws IOException {
		log_dir = handle_directory.Create_log_directory("Put_test3_log");
		requestbody = put_request_repository.put_request();
		endpoint = put_endpoint1.put_endpoint();

		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Put request with payload")
	public void send_the_put_request_with_payload() {
		statuscode = Comman_method_handle_api.put_statusCode(requestbody, endpoint);
		responsebody = Comman_method_handle_api.put_responsebody(requestbody, endpoint);

		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statuscode, 200);

		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Put  Response body parameters")
	public void validate_put_response_body_parameters() throws IOException {
		handle_api_logs.avidence_creator(log_dir, " put_test3", endpoint, requestbody, responsebody);
		Post_test1.validatur(requestbody, responsebody);
		System.out.println("Put_responscode is Sucssful");
		// throw new io.cucumber.java.PendingException();
	}
	@After
	public void teardown() {
		System.out.println("processEnded");
	}

}
