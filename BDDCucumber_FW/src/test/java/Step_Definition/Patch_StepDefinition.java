package Step_Definition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Api_commen_method.Comman_method_handle_api;
import New_repository.patch_request_repository;
import Test1_package.Post_test1;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.patch_endpoint;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_StepDefinition extends Comman_method_handle_api {
	File log_dir;
	String requestbody;
	String endpoint;
	String responsebody;
	int StatusCode;
	@Before()
	public void setup() {
		System.out.println("Triggering  Process Begins");
	}

	@Given("Enter NAME and JOB in Patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		log_dir = handle_directory.Create_log_directory("Patch_test2_log");
		requestbody = patch_request_repository.patch_request();
		endpoint = patch_endpoint.patch_endpoint();

		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() {
		int statuscode = Comman_method_handle_api.patch_statusCode(requestbody, endpoint);

		responsebody = Comman_method_handle_api.patch_responsebody(requestbody, endpoint);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(patch_statusCode(null, null), 201);

		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patch  Response body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		handle_api_logs.avidence_creator(log_dir, " patch_test2", endpoint, requestbody, responsebody);
		Post_test1.validatur(requestbody, responsebody);
		System.out.println("Patch_responscode is Sucssful");

		// throw new io.cucumber.java.PendingException();
	}
	@After
	public void teardown() {
		System.out.println("Process Ended");
	}

}
